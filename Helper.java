//This is a helper class for training.
//Methods for practice
import java.util.Scanner;

public class Helper{

    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args){

        int test1 = 12345;
        int test2 = 10;
        int test3 = 334;
        String test = "teste 123";
        System.out.println(spaceRemover(test));

    }

    /** This method cheks if a number is a palindrome (if it is
        the same number right to left). Returns true if it is.
        @param int number
        @return boolean
     */
    public static boolean isPalindrome(int number){
        int reverse = 0;
        int tester = Math.abs(number);
        while (tester > 0){
            int lastDigit = tester % 10;
            reverse = reverse * 10;
            reverse += lastDigit;
            tester = tester / 10;
        }
        System.out.println(number + " vs. " + reverse);
        return (Math.abs(number) == reverse);
    }

    /** This method reverts case of a String (upper to lower and lower to
        upper). Returns the reversed string.
        @param String toRevert
        @return String reversed
     */
    public static String reverseCase(String toRevert){
        String reversed = "";
        for (int i = 0; i < toRevert.length(); i++){
            if (Character.isLowerCase(toRevert.charAt(i))) {
                reversed += Character.toUpperCase(toRevert.charAt(i));
            }
            else reversed += Character.toLowerCase(toRevert.charAt(i));
        }
        return reversed;
    }

    /** Returns an int from the sum of the first and last digit of a passed int.
        If paramether is negative, returns -1. If it is only one digit, returns
        the some with itself.
        @param int number
        @return int sum
     */
    public static int sumFirstAndLastDigit(int number){
        if (number < 0) { return -1; }
        else if (number < 10) {return number + number;}
        int first = number;
        do {
            if ((first /= 10) < 10) { break; }
        } while (true);
        return first + (number % 10);
    }

    /** Returns an int with the sum of the even digits of a param number.
        If the number is negative, returns -1
        @param int number
        @return int sum
     */
    public static int getEvenDigitSum(int number){
        if (number < 0) { return -1; }
        else if (number < 10) { return number % 2 == 0 ? number : 0 ;}
        int evenDigit = number;
        int sum = 0;
        while (evenDigit > 0) {
            int digit = evenDigit % 10;
            if (digit % 2 == 0) {sum += digit;}
            evenDigit /= 10;
        }
        return sum;
    }

    /** Compares two numbers to check if both have a common digit.
        Numbers should be in the range of 10 - 99.
        @param int numA, int numB
        @return boolean
     */
    public static boolean hasSharedDigit(int numA, int numB){
        if ((numA < 10 || numA > 99) || (numB < 10 || numB > 99)) { return false;}
        String strA = Integer.toString(numA);
        String strB = Integer.toString(numB);
        for (int i = 0; i < strA.length(); i++){
            if (strB.contains(Character.toString(strA.charAt(i)))) { return true;}
        }
        return false;
    }

    /** Compares if three numbers within the range of 10 and 1000 have the same
        last digit.
        @param int numA, int numB, int, numC
        @return boolean
    */
    public static boolean hasSameLastDigit(int numA, int numB, int numC){
        if ((numA < 10 || numA > 1000) || (numB < 10 || numB > 1000)
            || (numC < 10 || numC > 1000)) { return false; }
        return ((numA % 10 == numB % 10) || (numB % 10 == numC % 10)
                || (numA % 10 == numC % 10));
    }

    /** Cheks if a number is within the range of 10 and 1000
        @param int number
        @return boolean
     */
    public static boolean isValid(int number){
        return (number > 9 && number < 1001);
    }

    /** Gets the greatest common divisor between two integers greater than 10
        @param int first, int second
        @return int
     */
    public static int getGreatestCommonDivisor(int first, int second){
        if (first < 10 || second < 10) {return -1;}
        int divisor = first < second ? first : second;
        while (((first % divisor) + (second % divisor)) != 0 ) {
            divisor--;
        }
        return divisor;
    }

    /** Prints all the factors of a passed integer, from one to itself
        @param int number
     */
    public static void printFactors(int number){
        if (number < 1) System.out.println("Invalid Value");
        for (int i = 1; i <= number; i++) {
            System.out.print(number % i == 0 ? i + " ": "");
        }
    }

    /** Checks if an integer is a perfect number (the sum of its factors is
        equal to the number)
        @param int number
        @return boolean
     */
    public static boolean isPerfectNumber(int number){
        if (number < 1) return false;
        int checker = 0;
        for (int i = 1; i < number; i++){
            if (number % i == 0) checker += i;
        }
        return checker == number ? true: false;
    }

    /** Checks if an integer year (YYYY) is a leap year and returns true or false
        @param int year
        @return boolean
     */
    public static boolean isLeapYear(int year){
        if (year < 1 || year > 9999) return false;
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) return true;
        else return false;
    }

    /** Returns an integer with the number of days in a given month of a given year
     * (MM) and (YYYY). Calls isLeapYear() to check for leap year
        @param int month, int year
        @return int
     */
    public static int getDaysInMonth(int month, int year){
        if (month < 1 || month > 12 || year < 1 || year > 9999) return -1;
        switch (month){
        case 1: return 31;
        case 2: return isLeapYear(year) ? 29:28;
        case 3: return 31;
        case 4: return 30;
        case 5: return 31;
        case 6: return 30;
        case 7: return 31;
        case 8: return 31;
        case 9: return 30;
        case 10: return 31;
        case 11: return 30;
        case 12: return 31;
        }
        return -1;
    }

    /** Prints a string with the digits of the number written in English
        Calls getDigitCount() and reverse()
        @param int number
     */
    public static void numberToWords(int number){
        if (number < 0) System.out.println("Invalid Value");
        else {
            int[] digits = new int[getDigitCount(number)];
            int reversed = reverse(number);
            String toPrint = "";
            for (int i = 0; reversed > 0; i++) {
                digits[i] = reversed % 10;
                reversed /= 10;
            }
            for (int toWord: digits){
                switch (toWord) {
                case 0: toPrint += "Zero ";
                    break;
                case 1: toPrint += "One ";
                    break;
                case 2: toPrint += "Two ";
                    break;
                case 3: toPrint += "Three ";
                    break;
                case 4: toPrint += "Four ";
                    break;
                case 5: toPrint += "Five ";
                    break;
                case 6: toPrint += "Six ";
                    break;
                case 7: toPrint += "Seven ";
                    break;
                case 8: toPrint += "Eight ";
                    break;
                case 9: toPrint += "Nine ";
                    break;
                }
            }
            System.out.println(toPrint.trim());
        }
    }

    /** Reverse the order of an integer (12345 = 54321)
        @param int number
        @return int
     */
    public static int reverse(int number){
        String toRevert = Integer.toString(Math.abs(number));
        int sign = Integer.signum(number);
        String reversed = "";
        for (int i = toRevert.length() -1; i >= 0; i--) {
            reversed += toRevert.charAt(i);
        }
        return Integer.parseInt(reversed) * sign;
    }

    /** Returns the number of digits of a given integer
        @param int number
        @return int
     */
    public static int getDigitCount(int number){
        if (number < 0) return -1;
        String tester = Integer.toString(number);
        return tester.length();
    }

    /** Returns true if a given number is a positive odd
        @param int number
        @return boolean
     */
    public static boolean isPositiveOdd(int number){
        return number < 0 ? false: number % 2 == 0 ? false: true;
    }

    /** Sums all the odd numbers in a given interval (including the end number)
        Calls isPositiveOdd()
        @param int start, int end
        @return int
     */
    public static int sumOdd(int start, int end){
        if (start > end || start < 0 || end < 0) return -1;
        int sum = 0;
        for (int i = 0; i <= (end - start); i++){
            sum += isPositiveOdd(start + i) ? (start + i): 0;
        }
        return sum;
    }

    /** This method takes three integers: bigCount = a package of 5kg,
        smallCount = a package of 1kg, goal = Xkg size of pack
        It calculates if it is possible to assemble at least one full goal pack
        with the given packages
        @param int bigCount, int smallCount, int goal
        @return boolean
     */
    public static boolean canPack(int bigCount, int smallCount, int goal){
        if (smallCount < 0 || bigCount < 0 || goal < 0) return false;
        int big = bigCount * 5;
        int spare = goal % 5;
        if (big + smallCount == goal) return true;
        else if (big == 0) {
            return smallCount >= goal ? true:false;
        }
        else if (big > 0){
            if (goal % 5 == 0){
                if (big > goal) return true;
                else if (smallCount >= (goal - big)) return true;
            }
            else {
                if (big > goal) {
                    if ((bigCount > 1 && goal > 1) && smallCount >= spare) return true;
                }
                else {
                    if (big + smallCount >= goal) return true;
                }
            }
        }
        return false;
    }

    /** Takes a double km/h and converts it to a long mi/h. Returns -1
        if the param is negative
        @param double kilometersPerHour
        @return long
     */
    public static long toMilesPerHour(double kilometersPerHour) {
        if (kilometersPerHour < 0) return -1;
        return Math.round(kilometersPerHour / 1.609);
    }

    /** Print the result of a km/h to a mi/h conversion. Calls toMilesPerHour()
        @param double kilometersPerHour
     */
    public static void printConversion(double kilometersPerHour){
        String printMe = "";
        if (kilometersPerHour < 0) printMe = "Invalid Value";
        else {
            long milesPerHour = toMilesPerHour(kilometersPerHour);
            printMe = kilometersPerHour + " km/h = " + milesPerHour + " mi/h";
        }
        System.out.println(printMe);
    }

    /** Prints an integer value of kilobytes in the form of MB and KB
        @param int kiloBytes
     */
    public static void printMegaBytesAndKiloBytes(int kiloBytes){
        if (kiloBytes < 0) System.out.println("Invalid Value");
        else {long MB = Math.round(kiloBytes / 1024);
            long KB = kiloBytes - (MB * 1024);
            System.out.println(kiloBytes + " KB = " + MB +" MB and " + KB + " KB");
        }
    }

    /** Returns true if dog is barking between 22:00 and 08:00
        @param boolean barking, int hourOfDay
        @return boolean
     */
    public static boolean shouldWakeUp(boolean barking, int hourOfDay){
        if (hourOfDay < 0 || hourOfDay > 23) return false;
        else if ((barking) && (hourOfDay < 8 || hourOfDay > 22)) return true;
        return false;
    }

    /** Compares two doubles for equality by three decimal places without rounding
        @param double num1, double num2
        @return boolean
     */
    public static boolean areEqualByThreeDecimalPlaces(double num1, double num2){
        double numA = num1*1000;
        double numB = num2*1000;
        return ((int)numA==(int)numB);
    }

    /** Returns true if the sum of num1 with num2 equals num3
        @param int num1, int num2, int num3
        @return boolean
     */
    public static boolean hasEqualSum(int num1, int num2, int num3){
        return (num1 + num2 == num3);
    }

    /** Compares three integers and returns true if any is in the range 13-19
        Comparison is made by calling isTeen()
        @param int num1, int num2, int num3
        @return boolean
     */
    public static boolean hasTeen(int num1, int num2, int num3){
        return (isTeen(num1) || isTeen(num2) || isTeen(num3));
    }

    /** Returns true if a number is the range 13-19
        @param int num
        @return boolean
     */
    public static boolean isTeen(int num){
        return (num >=13 && num <=19);
    }

    /** Calculates the area of a circle
        @param double radius
        @return double
     */
    public static double area(double radius){
        if (radius < 0) return -1.0;
        else {
            return (Math.PI * Math.pow(radius, 2));
        }
    }

    /** Calculates the area of a rectangle/square
        @param double width, double height
        @return double
     */
    public static double area(double width, double height){
        if (width < 0 || height < 0) return -1.0;
        else {
            return (width * height);
        }
    }

    /** Prints a String "X min = Y y and D days"
        @param long minutes
     */
    public static void printYearsAndDays(long minutes){
        if (minutes < 0) System.out.println("Invalid Value");
        else {
            int y = (int)(minutes / (60 * 24 * 365));
            int d = ((int)minutes - (y * 60 * 24 * 365)) / (60 * 24);
            System.out.println(minutes + " min = " + y + " y and " + d + " d");
        }
    }

    /** Returns true if summer is true and temperature between 25-45 or if
        summer is false and temperature is 25-35. Otherwise, false.
        @param boolean summer, int temperature
        @return boolean
     */
    public static boolean isCatPlaying(boolean summer, int temperature){
        if ((summer) && (temperature < 25 && temperature <= 45)) return true;
        else if ((!summer) && (temperature < 25 && temperature <= 35)) return true;
        else return false;
    }

    /** Returns the largest prime factor of a given integer. Returns -1 if there's
        no prime in the interval
        @param int number
        @return int
     */
    public static int getLargestPrime(int number){
        if (number <= 1) return -1;
        else if (number == 2) return number;
        for (int i = number -1; i > 1; i--){
            if (number % i == 0) {
                for (int x = 2; x <= i; x++){
                    if (i % x == 0 && i != x) break;
                    else if (i == x) return i;
                }
            }
            else if (number % i != 0 && i == 2) return number;
        }
        return -1;
    }

    /**Given 2 strings, a and b, return the number of the positions where they contain
       the same length 2 substring. So "xxcaazz" and "xxbaaz" yields 3, since the "xx",
       "aa", and "az" substrings appear in the same place in both strings.
       @param String a, String b
       @return int
     */
    public static int stringMatch(String a, String b){
        int len = Math.min(a.length(), b.length());
        int toReturn = 0;
        for (int i = 0; i < len -1; i++){
            String suba = a.substring(i, i+2);
            String subb = b.substring(i, i+2);
            toReturn += suba.equals(subb) ? 1 : 0;
        }
        return toReturn;
    }

    /**Given a string and a non-negative int n, return a larger string that is n copies of the original string.
       stringTimes("Hi", 2) = "HiHi"
       @param String str, int number
       @return String
     */
    public static String stringTimes(String str, int number){
        String toReturn = "";
        for (int i = 0; i < number; i++){
            toReturn += str;
        }
        return toReturn;
    }

    /**Given a non-empty String ("Test") return a new String with chars 0.01.012.etc ("TTeTesTest")
       @param String str
       @return String
     */
    public static String stringSplosion(String str){
        String toReturn = "";
        for (int i = 0; i < str.length(); i++){
            toReturn += str.substring(0, i+1);
        }
        return toReturn;
    }

    /**Checks if an array of int has the number 9
       @param int[] numbers
       @return boolean
     */
    public static boolean has9(int[] numbers){
        for (int n : numbers){
            if (n == 9) return true;
        }
        return false;
    }

    /**Checks is an array of ints has the number 9 in the first four elements
       @param int[] numbers
       @return boolean
     */
    public static boolean has9InFirst4(int[] numbers){
        for (int i = 0; i < numbers.length; i++){
            if (i >= 4) break;
            if (numbers[i] == 9) return true;
        }
        return false;
    }

    /**Removes "x" from a String, except if it's the first or last character
       @param String str
       @return String
     */
    public static String xRemover(String str){
        String toReturn = "";
        if (str.length() <= 2) {
            return str;
        }
        else {
            for (int i = 0; i<str.length(); i++){
                if (i == 0){
                    toReturn += str.substring(i,i+1);
                }
                else if (i > 0 && i < str.length()-1){
                    if (!str.substring(i, i+1).equals("x")){
                        toReturn += str.substring(i, i+1);
                    }
                }
                else toReturn += str.substring(i, i+1);
            }
        }
        return toReturn;
    }

    /** Prints a star box according to the param integer. The box rows and columns
        equals the param number. First and last line prints all columns. All rows
        prints first and last columns. Other rows prints stars when row equals column
        or when row equals (number - (row + 1))
        @param int number
     */
    public static void printSquareStar(int number){
        if (number < 5) System.out.println("Invalid Value");
        else {
            for (int row = 0; row < number; row++) {
                for (int col = 0; col < number; col ++){
                    if (row == 0 || row == number -1) System.out.print("*");
                    else if (row != 0 && row != number -1) {
                        if (col == 0 || col == number -1) System.out.print("*");
                        else if (col == row || col == number - (row + 1)) System.out.print("*");
                        else System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
    }

    /** Asks the user to input a number "n" times and prints the sum. Print an error
        message while wrong input
     */
    public static void printSumUserNumbers(){
        java.util.Scanner input = new java.util.Scanner(System.in);
        int ctr = 0;
        int sum = 0;
        while (ctr < 5){
            System.out.println("Enter #" + (ctr+1));
            boolean isInt = input.hasNextInt();
            if (isInt){
                int number = input.nextInt();
                ctr++;
                sum += number;
            } else {
                System.out.println("Invalid Number");
            }
            input.nextLine();
            input.close();
        }
    }

    /** Asks the user to type a number in an infinite loop. When the user types
        anything different than an integer, breaks the loop and prints the sum and
        average of the entered numbers
     */
    public static void inputThenPrintSumAndAverage(){
        java.util.Scanner input = new java.util.Scanner(System.in);
        int sum = 0;
        int ctr = 0;
        while(true){
            System.out.println("Enter a number: ");
            if (input.hasNextInt()) {
                sum += input.nextInt();
                ctr++;
            }
            else break;
        }
        System.out.println("SUM = " + sum + " AVG = " + Math.round((double)sum/(double)ctr));
        input.close();
    }

    /**Calculates how many buckets of paint are needed to paint a wall. Takes width,
       hight, area per bucket and extra buckets. Calls getBucketCount(width, height,
       areaPerBucket);
       @param double width, double height, double areaPerBucket, int extraBuckets
       @return int
     */
    public static int getBucketCount(double width, double height, double areaPerBucket, int extraBuckets){
        if (width <= 0 || height <= 0 || areaPerBucket <= 0 || extraBuckets < 0) return -1;
        return getBucketCount(width, height, areaPerBucket) - extraBuckets;
    }

    /**Calculates how many buckets of paint are needed to paint a wall. Takes width,
       hight and area per bucket. Calls getBucketCount(area, areaPerBucket);
       @param double width, double height, double areaPerBucket
       @return int
     */
    public static int getBucketCount(double width, double height, double areaPerBucket){
        if (width <= 0 || height <= 0 || areaPerBucket <= 0) return -1;
        return getBucketCount((width * height), areaPerBucket);
    }

    /**Calculates how many buckets of paint are needed to paint a wall. Takes area
       and area per bucket. Returns an integer with the number of necessary buckets
       @param double area, double areaPerBucket
       @return int
     */
    public static int getBucketCount(double area, double areaPerBucket){
        if (area <=0 || areaPerBucket <=0) return -1;
        return (int)Math.ceil(area/areaPerBucket);
    }

    /**Sorts an array of ints in descending order. It does not call any Arrays or
       Collections methods
       @param int[] numbers
       @return int[]
     */
    public static int[] sortIntegers(int[] numbers){
        int[] sorted = new int[numbers.length];
        //Built-in method to copy arrays
        //int[] sorted = java.util.Arrays.copyOf(numbers, numbers.length);
        for (int x = 0; x < sorted.length; x++) {
            sorted[x] = numbers[x];
        }
        //java.util.Arrays.sort(numbers);
        //java.util.Arrays.sort(numbers, java.util.Collections.reverseOrder());
        boolean loop = true;
        int temp;
        while (loop){
            loop = false;
            for(int i=0; i<sorted.length-1; i++){
                if(sorted[i] < sorted[i+1]){
                    temp = sorted[i];
                    sorted[i] = sorted[i+1];
                    sorted[i+1] = temp;
                    loop = true;
                }
            }
        }
        return sorted;
    }

    /**Finds the minimum value in an array of integers
       @param int[] numbers
       @return int
     */
    public static int findMin(int[] numbers){
        int min = Integer.MAX_VALUE;
        for (int i: numbers){
            min = i < min ? i:min;
        }
        return min;
    }

    /**Gets the user console input as an int. Checks for valid input and loops
       until a valid number is entered. Prints a String passed with the call.
       @param String msg
       @return int
     */
    public static int getUserInt(String msg){
        System.out.println(msg);
        int toReturn = 0;
        while (true){
            boolean isInt = in.hasNextInt();
            if (isInt){
                toReturn = in.nextInt();
                break;
            }
            else {
                System.out.println("Ivalid input\n" + msg);
                in.nextLine();
            }
        }
        in.nextLine();
        return toReturn;
    }

    /**Creates and returns an int[] array of the size passed in the call.
       Calls getUserInt() to assign an int to each element.
       @param int count
       @return int[]
     */
    public static int[] readIntegers(int count){
        int[] numbers = new int[count];
        for (int i = 0; i<count; i++){
            numbers[i] = getUserInt("#"+i+":");
        }
        return numbers;
    }

    /**Reverts the order of an Array of Integers.
       @param int[] numbers
     */
    public static void reverseIntArray(int[] numbers){
        for (int i=0; i<numbers.length/2; i++){
            int temp = numbers[i];
            numbers[i] = numbers[numbers.length - (i+1)];
            numbers[numbers.length - (i+1)] = temp;
        }
    }

    /**Removes a String(case insensitive) from a base String
       @param String base, String remove
       @return String
     */
    public static String stringRemover(String base, String remove){
        String clean = "";
        String[] splited = base.split( "(?i)"+remove);
        for (int i=0; i<splited.length;i++){
            clean += splited[i];
        }
        return clean;
    }

    /** Returns true if a string contains a number in the folllowing formats:
        NNNNNNNNNN, NNN-NNN-NNNN, NNN.NNN.NNNN, NNN NNN NNNN. It also returns true
        if the first number is enclosed in parentheses and if there is a 3-5 number
        extension in the format xNNN, extNNN
     */
    public static boolean isPhoneNumber(String phoneNumber){
        if (phoneNumber.matches("\\d{10}")) return true;
        else if (phoneNumber.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
        else if (phoneNumber.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
        else if (phoneNumber.matches("\\(\\d{3}\\)[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
        else if (phoneNumber.matches("\\(\\d{3}\\)[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
        return false;
    }

    /** Returns the nth fibonacci number
        @param int n
        @return long
     */
    public static long fibonacci(int n){
        if (n == 0) return 0;
        else if (n==1) return 1;
        long nMinus1 = 1;
        long nMinus2 = 0;
        long fib = 0;
        for (int i = 1; i < n; i++){
            fib = (nMinus1)+(nMinus2);
            nMinus2 = nMinus1;
            nMinus1 = fib;
        }
        return fib;
    }

    /**
     * Takes a String and replaces spaces with a "+" sign
     * @return String
     */
    public static String spaceRemover(String str){
        String toReturn = "";
        str.trim();
        String[] splited = str.split(" ");
        for (int i=0; i<splited.length;i++){
            toReturn += splited[i];
            if (i < splited.length -1) {
                toReturn += "+";
            }
        }
        return toReturn;
    }

    /**
     * Takes a list of Numbers and returns an int with how many odd numbers are in the list
     * @return int
     */
    public static <T extends Number> int howManyOdds(List<T> list){
        int count = 0;
        for (T i: list) {
            double x = i.doubleValue();
            if (x % 2.0 != 0) {
                count++;
            }
        }
        return count;
    }

    /**
     * Generic method to replace elements in a List
     */
    public static <T extends Object> void swapElements(List<T> list, int indexA, int indexB){
        if (indexA >= list.size() || indexB >= list.size() || indexA < 0 || indexB < 0) {
            throw new IndexOutOfBoundsException("Index does not exist");
        }
        else {
            T holdMe = list.get(indexA);
            list.set(indexA, list.get(indexB));
            list.set(indexB, holdMe);
        }
    }

    /**
       Generic method to print all elements of an array
     */
    public static <T> void printArray(T[] arrayToPrint) {
        for (T i: arrayToPrint) {
            System.out.println(i);
        }
    }

    /**
       Generic method that returns a T of higher value from a Comparable test of objects in a defined range of a list
       @return T
     */
    public static <T extends Object & Comparable<? super T>> T findMaxInRange(List<? extends T> list, int begin, int end) {
        T max = list.get(begin);
        for (++begin; begin < end; ++begin) {
            if (max.compareTo(list.get(begin)) < 0)
                max = list.get(begin);
        }
        return max;
    }


}
